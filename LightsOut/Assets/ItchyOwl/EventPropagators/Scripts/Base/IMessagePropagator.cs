﻿namespace ItchyOwl.Propagators
{
    /// <summary>
    /// Common interface for all components that propagate Unity callbacks.
    /// </summary>
    public interface IMessagePropagator { }
}
