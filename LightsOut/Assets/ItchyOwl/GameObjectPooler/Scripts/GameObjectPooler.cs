﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ItchyOwl.Propagators;

namespace ItchyOwl.ObjectPooling
{
    public static class GameObjectPooler
    {
        /// <summary>
        /// Enable to see the debug logs.
        /// </summary>
        public static bool debug;

        // Game object pools, where the key is the prefab and the value is the instance.
        private static Dictionary<GameObject, GameObjectPool> pools = new Dictionary<GameObject, GameObjectPool>();

        // Links from game object instances to the prefab. The keys are instances and the values are prefabs. Each instance should have a unique link.
        private static Dictionary<GameObject, GameObject> prefabLinks = new Dictionary<GameObject, GameObject>();

        public static event Action<GameObjectPool> PoolCreated;
        public static event Action<GameObjectPool> PoolRemoved;

        #region Queries
        public static int PoolCount { get { return pools.Count; } }

        public static int TotalObjectCount { get { return prefabLinks.Count; } }

        /// <summary>
        /// Uses Linq Sum -> Don't use too frequently. You can also access the pools directly in order to not generate any garbage.
        /// </summary>
        public static int ActiveInstanceCount { get { return pools.Sum(p => p.Value.ActiveInstanceCount); } }

        /// <summary>
        /// Uses Linq Sum -> Don't use too frequently. You can also access the pools directly in order to not generate any garbage.
        /// </summary>
        public static int InactiveInstanceCount { get { return pools.Sum(p => p.Value.InactiveInstanceCount); } }

        /// <summary>
        /// This method gets all the objects of all pools via Linq SelectMany and therefore generates garbage -> Don't use too frequently.
        /// </summary>
        public static IEnumerable<GameObject> GetAllObjects() { return GetActiveObjects().Concat(GetInactiveObjects()); }

        /// <summary>
        /// This method gets the active objects of all pools via Linq SelectMany and therefore generates garbage -> Don't use too frequently.
        /// </summary>
        public static IEnumerable<GameObject> GetActiveObjects() { return pools.SelectMany(p => p.Value.ActiveInstances); }

        /// <summary>
        /// This method gets the inactive objects of all pools via Linq SelectMany and therefore generates garbage -> Don't use too frequently.
        /// </summary>
        public static IEnumerable<GameObject> GetInactiveObjects() { return pools.SelectMany(p => p.Value.InactiveInstances); }
        #endregion

        #region Extension methods for GameObjects
        /// <summary>
        /// If no pool exists, creates a new pool for the prefab.
        /// For the best performance, don't set the parent!
        /// </summary>
        public static GameObjectPool GetPool(this GameObject prefab, Transform parent = null, bool setParent = true)
        {
            GameObjectPool pool;
            if (!pools.TryGetValue(prefab, out pool))
            {
                pool = CreateNewPoolFor(prefab, parent, setParent);
            }
            return pool;
        }

        /// <summary>
        /// Instantiates, inactivates, and pools the game objects for later usage.
        /// For the best performance, don't set the parent!
        /// </summary>
        public static List<GameObject> Prefabricate(this GameObject prefab, int count, Transform parent = null, bool setParent = true)
        {
            if (debug)
            {
                Debug.LogFormat("[GameObjectPooler] Prefabricating {0} instances of {1}", count, prefab.name);
            }
            GameObjectPool pool = GetPool(prefab, parent, setParent);
            List<GameObject> instances = new List<GameObject>();
            for (int i = 0; i < count; i++)
            {
                instances.Add(pool.CreateNewInstance(startActive: false));
            }
            return instances;
        }

        /// <summary>
        /// Returns a pooled object if found and activates it. If no pooled instances are found, a new instance is created and pooled.
        /// For the best performance, don't set the parent!
        /// </summary>
        public static GameObject Spawn(this GameObject prefab, Transform parent = null, bool setParent = true)
        {
            return GetPool(prefab, parent, setParent).Pop();
        }

        /// <summary>
        /// Recycles the instance back to the object pool.
        /// NOTE: Only works if the object has been created via Spawn or Prefabricate methods!
        /// </summary>
        public static bool Recycle(this GameObject instance, Action<GameObject> callback = null)
        {
            GameObject prefab;
            if (!prefabLinks.TryGetValue(instance, out prefab))
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot recycle instance because cannot find the reference to the prefab. Was the object created via GameObjectPooler (Spawn or Prefabricate methods) or pool already destroyed?");
                return false;
            }
            GameObjectPool pool;
            if (!pools.TryGetValue(prefab, out pool))
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot recycle instance because cannot find the pool for the prefab. Was the object created via GameObjectPooler (Spawn or Prefabricate methods) or pool already destroyed?");
                return false;
            }
            pool.Inactivate(instance, callback);
            return true;
        }

        /// <summary>
        /// Recycles all the instances of the given prefab back to the object pool.
        /// </summary>
        public static void RecycleAll(this GameObject prefab, Action<GameObject> callbackPerObject = null)
        {
            GameObjectPool pool;
            if (TryGetPoolForPrefab(prefab, out pool))
            {
                pool.InactivateAll(callbackPerObject);
            }
            else
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot find a pool for {0}!", prefab.name);
            }
        }

        /// <summary>
        /// Releases and destroys pooled inactive instances.
        /// If no count is specified, releases all inactives.
        /// Use this method for prefabs.
        /// </summary>
        public static int ReleaseInactivesOfPrefab(this GameObject prefab, int count = 0)
        {
            if (debug)
            {
                if (count == 0)
                {
                    Debug.LogFormat("[GameObjectPooler] Trying to release all inactive instances of {0}", prefab.name);
                }
                else
                {
                    Debug.LogFormat("[GameObjectPooler] Trying to release {0} inactive instances of {1}", count, prefab.name);
                }
            }
            GameObjectPool pool;
            if (!pools.TryGetValue(prefab, out pool))
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot release inactive instances of {0} because cannot find the pool for the prefab. Was the object created via GameObjectPooler (Spawn or Prefabricate methods)?", prefab.name);
                return 0;
            }
            int released = pool.ReleaseInactives(count);
            if (debug)
            {
                Debug.LogFormat("[GameObjectPooler] Inactives of {0} released: {1}", prefab.name, released);
            }
            return released;
        }

        /// <summary>
        /// Releases and destroys pooled instances that has the same prefab link than this instance.
        /// If no count is specified, releases all inactives.
        /// Use this method for instances.
        /// </summary>
        public static int ReleaseInactives(this GameObject instance, int count = 0)
        {
            if (debug)
            {
                Debug.LogFormat("[GameObjectPooler] Trying to release {0} inactive instances of {1}", count, instance.name);
            }
            GameObject prefab;
            if (!prefabLinks.TryGetValue(instance, out prefab))
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot release inactive instances of {0} because cannot find the reference to the prefab. Was the object created via GameObjectPooler (Spawn or Prefabricate methods)?", instance.name);
                return 0;
            }
            GameObjectPool pool;
            if (!pools.TryGetValue(prefab, out pool))
            {
                Debug.LogWarningFormat("[GameObjectPooler] Cannot release inactive instances of {0} because cannot find the pool for the prefab. Was the object created via GameObjectPooler (Spawn or Prefabricate methods)?", prefab.name);
                return 0;
            }
            int released = pool.ReleaseInactives(count);
            if (debug)
            {
                Debug.LogFormat("[GameObjectPooler] Inactives of {0} released: {1}", prefab.name, released);
            }
            return released;
        }
        #endregion

        #region Pool managing
        public static bool TryGetPoolForPrefab(GameObject prefab, out GameObjectPool pool)
        {
            return pools.TryGetValue(prefab, out pool);
        }

        public static bool TryGetPoolForInstance(GameObject instance, out GameObjectPool pool)
        {
            GameObject prefab;
            pool = null;
            if (!prefabLinks.TryGetValue(instance, out prefab)) { return false; }
            if (!pools.TryGetValue(prefab, out pool)) { return false;}
            return pool != null;
        }

        private static GameObjectPool CreateNewPoolFor(GameObject prefab, Transform parent = null, bool setParent = true)
        {
            var poolGO = new GameObject(prefab.name + " Object Pool");
            var pool = poolGO.AddComponent<GameObjectPool>();
            // Remove the object pool when it's destroyed.
            poolGO.AddComponent<OnDestroyPropagatorN>().Destroyed += (sender, args) => RemoveObjectPool(pool);
            // Add the link to the prefab when a new instance is created.
            pool.ObjectCreated += go => prefabLinks.Add(go, prefab);
            // Remove the link when the instance is destroyed.
            pool.ObjectDestroyed += go => prefabLinks.Remove(go);
            pool.prefab = prefab;
            var inactiveObjects = new GameObject("Inactive");
            var activeObjects = new GameObject("Active");
            if (parent as RectTransform != null)
            {
                poolGO.AddComponent<RectTransform>();
                inactiveObjects.gameObject.AddComponent<RectTransform>();
                activeObjects.gameObject.AddComponent<RectTransform>();
            }
            if (parent != null)
            {
                poolGO.transform.SetParent(parent, worldPositionStays: false);
            }
            inactiveObjects.transform.SetParent(poolGO.transform, worldPositionStays: false);
            activeObjects.transform.SetParent(poolGO.transform, worldPositionStays: false);
            if (setParent)
            {
                pool.inactiveParent = inactiveObjects.transform;
                pool.activeParent = activeObjects.transform;
            }    
            pools.Add(prefab, pool);
            if (debug)
            {
                Debug.LogFormat("[GameObjectPooler] Pool created for {0}", prefab.name);
            }
            if (PoolCreated != null)
            {
                PoolCreated(pool);
            }
            return pool;
        }

        private static bool RemoveObjectPool(GameObjectPool pool)
        {
            if (pools.ContainsValue(pool))
            {
                pools.Remove(pool.prefab);
                if (debug)
                {
                    Debug.LogFormat("[GameObjectPooler] Pool {0} removed", pool.name);
                }
                if (PoolRemoved != null)
                {
                    PoolRemoved(pool);
                }
                return true;
            }
            else
            {
                Debug.LogWarningFormat("GameObjectPooler] Cannot remove pool {0}, because cannot find it from the collection.", pool.name);
                return false;
            }
        }
        #endregion
    }
}

