﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using ItchyOwl.Auxiliary;

namespace ItchyOwl.ObjectPooling.Examples
{
    public class PoolerExample : MonoBehaviour
    {
        public bool debug;
        public PrefabSpawner spawner;
        [Tooltip("How many instances of the prefab are prefabricated?")]
        public int bufferSize = 10;
        public List<GameObject> prefabs = new List<GameObject>();

        public Text poolsText;
        public Text totalObjectsText;
        public Text activeObjectsText;
        public Text inactiveObjectsText;

        public Button releaseButton;
        public Button recycleButton;

        private int activeObjectCount;
        private int inactiveObjectCount;

        private List<GameObjectPool> pools = new List<GameObjectPool>();

        private void Awake()
        {
            if (bufferSize < 0) { bufferSize = 0; }
            GameObjectPooler.debug = debug;
            GameObjectPooler.PoolCreated += OnPoolCreated;
            GameObjectPooler.PoolRemoved += OnPoolRemoved;
            releaseButton.onClick.AddListener(() =>
            {
                prefabs.ForEach(prefab => GameObjectPooler.ReleaseInactivesOfPrefab(prefab));
                RecalculateObjectCount();
                //UpdateObjectCount();
            });
            recycleButton.onClick.AddListener(() =>
            {
                // Temporarily unsubscribe from the events, in order to prevent multiple calls to UpdateObjectCount().
                pools.ForEach(p => UnsubscribeFromPoolEvents(p));
                // Recycle
                prefabs.ForEach(prefab => GameObjectPooler.RecycleAll(prefab));
                // Re-subscribe to the events
                pools.ForEach(p => SubscribeToPoolEvents(p));
                // Recalculate and update
                RecalculateObjectCount();
                //UpdateObjectCount();
            });
        }

        private void Start()
        {
            prefabs.ForEach(p => p.Prefabricate(bufferSize));
            if (spawner != null)
            {
                spawner.prefabs.AddRange(prefabs);
            }
        }

        private void Update()
        {
            // Usually it's bad idea to update text per frame, because manipulating strings generate garbage.
            // In this context, however, there are multiple objects activated and inactivated per frame, so it's better to update the object count _only_ once per frame
            // Feel free to test it: Comment out this function and uncomment the others found in this script.
            UpdateObjectCount();
        }

        private void OnDestroy()
        {
            GameObjectPooler.PoolCreated -= OnPoolCreated;
            GameObjectPooler.PoolRemoved -= OnPoolRemoved;
        }

        private void OnPoolCreated(GameObjectPool pool)
        {
            SubscribeToPoolEvents(pool);
            pools.Add(pool);
            UpdatePoolCount();
        }

        private void OnPoolRemoved(GameObjectPool pool)
        {
            UnsubscribeFromPoolEvents(pool);
            pools.Remove(pool);
            UpdatePoolCount();
        }

        private void SubscribeToPoolEvents(GameObjectPool pool)
        {
            pool.ObjectActivated += OnObjectActivated;
            pool.ObjectInactivated += OnObjectInactivated;
        }

        private void UnsubscribeFromPoolEvents(GameObjectPool pool)
        {
            pool.ObjectActivated -= OnObjectActivated;
            pool.ObjectInactivated -= OnObjectInactivated;
        }

        private void OnObjectActivated(GameObject go)
        {
            if (inactiveObjectCount > 0)
            {
                inactiveObjectCount--;
            }
            activeObjectCount++;
            //UpdateObjectCount();
        }

        private void OnObjectInactivated(GameObject go)
        {
            if (activeObjectCount > 0)
            {
                activeObjectCount--;
            }
            inactiveObjectCount++;
            //UpdateObjectCount();
        }

        private void RecalculateObjectCount()
        {
            activeObjectCount = GameObjectPooler.GetActiveObjects().Count();
            inactiveObjectCount = GameObjectPooler.GetInactiveObjects().Count();
        }

        private void UpdatePoolCount()
        {
            if (poolsText != null)
            {
                poolsText.text = "Pool count: " + GameObjectPooler.PoolCount;
            }
        }

        private void UpdateObjectCount()
        {
            if (totalObjectsText != null)
            {
                totalObjectsText.text = "Total objects: " + (activeObjectCount + inactiveObjectCount);
            }
            if (activeObjectsText != null)
            {
                activeObjectsText.text = "Active objects: " + activeObjectCount;
            }
            if (inactiveObjectsText != null)
            {
                inactiveObjectsText.text = "Inactive objects: " + inactiveObjectCount;
            }
        }
    }
}
