﻿using UnityEngine;
using UnityEngine.UI;
using ItchyOwl.Auxiliary;

namespace ItchyOwl.ObjectPooling.Examples
{
    [RequireComponent(typeof(Slider))]
    public class SpawnMultiplierSlider : MonoBehaviour
    {
        public PrefabSpawner spawner;

        private void Start()
        {
            var slider = GetComponent<Slider>();
            slider.value = spawner.multiplier;
            slider.onValueChanged.AddListener(v => spawner.multiplier = (int)v);
        }
    }
}
