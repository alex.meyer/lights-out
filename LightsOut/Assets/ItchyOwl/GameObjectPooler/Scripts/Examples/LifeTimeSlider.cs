﻿using UnityEngine;
using UnityEngine.UI;
using ItchyOwl.Auxiliary;
using ItchyOwl.Extensions;

namespace ItchyOwl.ObjectPooling.Examples
{
    [RequireComponent(typeof(Slider))]
    public class LifeTimeSlider : MonoBehaviour
    {
        public PoolerExample poolerController;

        private void Awake()
        {
            var slider = GetComponent<Slider>();
            poolerController.prefabs.ForEach(prefab => SetLifeTime(prefab, slider.value));
            slider.onValueChanged.AddListener(v => poolerController.prefabs.ForEach(prefab =>
            {
                SetLifeTime(prefab, v);
                GameObjectPool pool;
                if (GameObjectPooler.TryGetPoolForPrefab(prefab, out pool))
                {
                    pool.InactiveInstances.ForEach(i => SetLifeTime(i, v));
                    pool.ActiveInstances.ForEach(i => SetLifeTime(i, v));
                }
            }));
        }

        private void SetLifeTime(GameObject go, float lifeTime)
        {
            var autoDestruction = go.GetComponent<AutoDestruction>();
            if (autoDestruction != null)
            {
                autoDestruction.lifeTime = lifeTime;
            }
        }
    }
}
