using UnityEngine;

namespace CoreGame
{
    [RequireComponent(typeof(Camera))]
    public class CameraToDataSO : MonoBehaviour
    {
        [SerializeField] private CameraDataSO cameraDataSO;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            cameraDataSO.Value = GetComponent<Camera>();
        }

        private void OnDisable()
        {
            RegisterListeners(false);
        }

        private void RegisterListeners(bool register)
        {
            if (!register) return;
        }

        private void ValidateReferences()
        {
            Debug.Assert(cameraDataSO != null, "cameraDataSO != null");
        }

    }
}