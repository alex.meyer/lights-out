using UnityEngine;
using UnityEngine.EventSystems;

namespace CoreGame
{
    [RequireComponent(typeof(RectTransform))]
    public class DetectScreenSizeChange : UIBehaviour
    {
        [Header("EventSOs")]
        [SerializeField] private EventSO screenResizeEventSo;

        protected override void Awake()
        {
            ValidateReferences();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange ();

            screenResizeEventSo.Invoke();

            // Debug.Log($"[SCREEN][SIZE] Screen Size Changed => Event thrown...");
        }

        private void ValidateReferences()
        {
            Debug.Assert(screenResizeEventSo != null, "screenResizeEventSo != null");
        }
    }
}