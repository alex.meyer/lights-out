using UnityEngine;
using ItchyOwl.Extensions;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

namespace CoreGame
{
    [RequireComponent(typeof(RectTransform))]
    public class RectTransformDimensionToWorldUnits : UIBehaviour
    {
        [Title("DataSOs")]
        [Required]
        [SerializeField] private Vector2DataSO sizeInWorldUnitsDataSo;
        [Required]
        [SerializeField] private Vector2DataSO positionInWorldUnitsDataSo;

        private RectTransform rectTransform;

        private RectTransform Rect
        {
            get
            {
                if (rectTransform == null)
                {
                    rectTransform = GetComponent<RectTransform>();
                }

                return rectTransform;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            ValidateReferences();
        }

        private void LayoutChanged()
        {
            var worldDimensions = Rect.GetWorldRect();
            worldDimensions.size = VecHelp.Abs(worldDimensions.size);

            // Debug.Log($"[AL] Layout of \"{gameObject.name}\" changed set world units pos: {worldDimensions.center}, size: {worldDimensions.size}");

            sizeInWorldUnitsDataSo.Set(worldDimensions.size);
            positionInWorldUnitsDataSo.Set(worldDimensions.center);
        }

        protected override void OnRectTransformDimensionsChange()
        {
            LayoutChanged();
        }

        private void ValidateReferences()
        {
            Debug.Assert(sizeInWorldUnitsDataSo != null, "sizeInWorldUnitsDataSo != null");
            Debug.Assert(positionInWorldUnitsDataSo != null, "positionInWorldUnitsDataSo != null");
        }
    }
}
