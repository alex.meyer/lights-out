using UnityEngine;

namespace CoreGame
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaPanelController : MonoBehaviour
    {
        [Header("DataSOs")]
        [SerializeField] private RectDataSO safeAreaInScreenUnitsDataSo;

        [Header("Components (dynamically set)")]
        [SerializeField] private RectTransform rectTrans;

        private RectTransform RectTrans
        {
            get
            {
                if (rectTrans == null)
                {
                    rectTrans = GetComponent<RectTransform>();
                }

                return rectTrans;
            }
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            SafeAreaChanged(Screen.safeArea);
        }

        private void OnDisable()
        {
            RegisterListeners(false);
        }

        private void SafeAreaChanged(Rect safeArea)
        {
            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;

            var screenSize = new Vector2(Screen.width, Screen.height);
            anchorMin /= screenSize;
            anchorMax /= screenSize;

            RectTrans.anchorMin = anchorMin;
            RectTrans.anchorMax = anchorMax;

            // Debug.Log($"[SAFE][AREA] safe area changed: {safeArea}");
        }

        private void RegisterListeners(bool register)
        {
            safeAreaInScreenUnitsDataSo.RegisterListener(SafeAreaChanged, register);
        }

        private void ValidateReferences()
        {
            Debug.Assert(safeAreaInScreenUnitsDataSo != null, "safeAreaInScreenUnitsDataSo != null");
        }
    }
}