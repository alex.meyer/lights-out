using UnityEngine;

namespace CoreGame
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaChangeDetection : MonoBehaviour
    {
        [Header("DataSOs")]
        [SerializeField] private RectDataSO safeAreaInWorldUnitsDataSo;
        [SerializeField] private RectDataSO safeAreaInScreenUnitsDataSo;
        [SerializeField] private RectDataSO safeAreaInCanvasUnitsDataSo;
        [SerializeField] private CameraDataSO mainCameraDataSo;

        [Header("EventSOs")]
        [SerializeField] private EventSO screenSizeChangedEventSo;

        [Header("Components")]
        [SerializeField] private RectTransform rectTrans;
        [SerializeField] private Canvas mainCanvas;
        [SerializeField] private RectTransform mainCanvasRectTrans;

        private RectTransform RectTrans
        {
            get
            {
                if (rectTrans == null)
                {
                    rectTrans = GetComponent<RectTransform>();
                }

                return rectTrans;
            }
        }

        private Canvas MainCanvas
        {
            get
            {
                if (mainCanvas == null)
                {
                    mainCanvas = GetComponentInParent<Canvas>().rootCanvas;
                }

                return mainCanvas;
            }
        }

        private RectTransform MainCanvasRectTrans
        {
            get
            {
                if (mainCanvasRectTrans == null)
                {
                    mainCanvasRectTrans = MainCanvas.GetComponent<RectTransform>();
                }

                return mainCanvasRectTrans;
            }
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            ScreenSizeChanged();
        }

        private void OnDisable()
        {
            RegisterListeners(false);
        }

        private void ScreenSizeChanged()
        {
            if (mainCameraDataSo.Value == null)
            {
                return;
            }

            var safeArea = Screen.safeArea;
            safeAreaInScreenUnitsDataSo.Set(safeArea);
            var positionInWorldUnits = mainCameraDataSo.Value.ScreenToWorldPoint(safeArea.position);
            var otherPositionInWorldUnits =
                mainCameraDataSo.Value.ScreenToWorldPoint(safeArea.position + safeArea.size);
            var worldRect = new Rect(positionInWorldUnits, otherPositionInWorldUnits - positionInWorldUnits);
            safeAreaInWorldUnitsDataSo.Set(worldRect);
            var positionInCanvas = ConvertPositionsHelper
                .WorldPositionToAnchoredPositionInRectangle(mainCameraDataSo.Value, positionInWorldUnits,
                    MainCanvasRectTrans);
            var otherPositionInCanvas = ConvertPositionsHelper
                .WorldPositionToAnchoredPositionInRectangle(mainCameraDataSo.Value, otherPositionInWorldUnits,
                    MainCanvasRectTrans);
            var canvasRect = new Rect(positionInCanvas, otherPositionInCanvas - positionInCanvas);
            safeAreaInCanvasUnitsDataSo.Set(canvasRect);

            // Debug.Log(
            //     $"[SCREEN][SAFE][AREA] Detected new safe area in screen: {safeArea}, in world: {worldRect}, in canvas: {canvasRect}");
        }

        private void RegisterListeners(bool register)
        {
            screenSizeChangedEventSo.RegisterListener(ScreenSizeChanged, register);
        }

        private void ValidateReferences()
        {
            Debug.Assert(safeAreaInWorldUnitsDataSo != null, "safeAreaInWorldUnitsDataSo != null" );
            Debug.Assert(safeAreaInCanvasUnitsDataSo != null, "safeAreaInCanvasUnitsDataSo != null");
            Debug.Assert(safeAreaInScreenUnitsDataSo != null, "safeAreaInScreenUnitsDataSo != null");
            Debug.Assert(mainCameraDataSo != null, "mainCameraDataSo != null");
        }
    }
}