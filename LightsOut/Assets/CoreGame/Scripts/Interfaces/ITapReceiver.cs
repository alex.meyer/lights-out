namespace CoreGame
{
    public interface ITapReceiver
    {
        void Tapped();
    }
}
