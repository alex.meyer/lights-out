using UnityEngine;

namespace CoreGame
{
    public static class ConvertPositionsHelper
    {
        public static Vector2 LocalPositionToAnchoredPosition(Camera cam, Vector2 localPos, Transform worldParent, RectTransform uiParent)
        {
            var worldPos = worldParent.TransformPoint(localPos);
            return WorldPositionToAnchoredPositionInRectangle(cam, worldPos, uiParent);
        }

        public static Vector2 WorldPositionToAnchoredPositionInRectangle(Camera cam, Vector2 worldPos, RectTransform parent)
        {
            var screenPoint = RectTransformUtility.WorldToScreenPoint(cam, worldPos);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    parent,
                    screenPoint,
                    cam,
                    out var anchoredPosition);
            return anchoredPosition;
        }

        public static Vector2 ScreenPointToLocalPosition(Camera cam, Vector2 screenPosition, Transform parent)
        {
            var  worldPosition = cam.ScreenToWorldPoint(screenPosition);
            return parent.InverseTransformPoint(worldPosition);
        }

        public static Vector2 ScreenPointToAnchoredPosition(Camera cam, Vector2 screenPoint, RectTransform parent)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                parent,
                screenPoint,
                cam,
                out var position
            );

            return position;
        }

        public static Vector2 GetScreenSizeInWorldUnits(Camera cam)
        {
            return cam.ViewportToWorldPoint(Vector2.one) * 2;
        }

        public static Vector2 WorldToLocalScale(this Transform transform, Vector2 scale)
        {
            Transform parent = transform.parent;

            while (parent != null)
            {
                scale /= parent.localScale;
                parent = parent.parent;
            }

            return scale;
        }

        public static Vector2 LocalToWorldScale(this Transform transform, Vector2 scale)
        {
            Transform parent = transform.parent;

            while (parent != null)
            {
                scale = Vector3.Scale(scale, parent.localScale);
                parent = parent.parent;
            }

            return scale;
        }
        
        public static Vector3 WorldToCanvasPosition(this Canvas canvas, Vector3 worldPosition, Camera camera = null)
        {
            if (camera == null)
            {
                camera = Camera.main;
            }
            var viewportPosition = camera.WorldToViewportPoint(worldPosition);
            return canvas.ViewportToCanvasPosition(viewportPosition);
        }

        public static Vector3 ScreenToCanvasPosition(this Canvas canvas, Vector3 screenPosition)
        {
            var viewportPosition = new Vector3(screenPosition.x / Screen.width,
                screenPosition.y / Screen.height,
                0);
            return canvas.ViewportToCanvasPosition(viewportPosition);
        }

        public static Vector3 CanvasToViewportPosition(this Canvas canvas, Vector3 canvasPos)
        {
            var canvasRect = canvas.GetComponent<RectTransform>();
            var scale = canvasRect.sizeDelta;
            scale.x = 1 / scale.x;
            scale.y = 1 / scale.y;
            var centerBasedViewPortPosition = Vector3.Scale(canvasPos, scale);
            var viewPortPosition = centerBasedViewPortPosition + new Vector3(0.5f, 0.5f, 0f);
            return viewPortPosition;
        }

        public static Vector3 ViewportToCanvasPosition(this Canvas canvas, Vector3 viewportPosition)
        {
            var centerBasedViewPortPosition = viewportPosition - new Vector3(0.5f, 0.5f, 0);
            var canvasRect = canvas.GetComponent<RectTransform>();
            var scale = canvasRect.sizeDelta;
            return Vector3.Scale(centerBasedViewPortPosition, scale);
        }
    }
}
