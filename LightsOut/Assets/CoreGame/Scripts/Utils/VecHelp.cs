using UnityEngine;

namespace CoreGame
{
    public static class VecHelp
    {
        public static Vector2 Abs(Vector2 vec)
        {
            return new Vector2(Mathf.Abs(vec.x), Mathf.Abs(vec.y));
        }
    }
}