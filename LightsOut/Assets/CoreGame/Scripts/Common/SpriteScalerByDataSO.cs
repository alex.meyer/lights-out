using Sirenix.OdinInspector;
using UnityEngine;

namespace CoreGame
{
    public class SpriteScalerByDataSO : MonoBehaviour
    {
        [Title("DataSOs")]
        [Required]
        [SerializeField] private Vector2DataSO sizeInWorldUnitsDataSo;

        [Title("Components")]
        [SerializeField] private SpriteRenderer spriteRenderer;

        [Title("Meta Data")]
        [SerializeField] private bool includeParentScale;
        [SerializeField] private bool keepAspectRatio;

        [Title("Dynamic Meta Data")]
        [SerializeField] private Vector2 textureSize;
        [SerializeField] private float pixelsPerUnit;

        private IParentScaleProvider _parentScaleProvider;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            Init();
            SizeWorldUnitsChanged(sizeInWorldUnitsDataSo.Value);
        }

        private void OnDisable()
        {
            RegisterListeners(false);
        }

        private void Init()
        {
            if (spriteRenderer.sprite == null)
            {
                return;
            }

            var sprite = spriteRenderer.sprite;
            textureSize = sprite.rect.size;
            pixelsPerUnit = sprite.pixelsPerUnit;
        }

        private void SizeWorldUnitsChanged(Vector2 newSizeInWorldUnits)
        {
            // the size of the sprite at scale factor 1
            var spriteSizeInWorldUnits = textureSize / pixelsPerUnit;
            // the scale is desired divides by norm size
            var worldScale = newSizeInWorldUnits / spriteSizeInWorldUnits;
            if (keepAspectRatio)
            {
                var worldScaleX = newSizeInWorldUnits.x / spriteSizeInWorldUnits.x;
                var worldScaleY = newSizeInWorldUnits.y / spriteSizeInWorldUnits.y;
                var scale = Mathf.Max(worldScaleX, worldScaleY);
                worldScale = new Vector2(scale, scale);
            }
            transform.localScale = worldScale;
        }

        private void RegisterListeners(bool register)
        {
            sizeInWorldUnitsDataSo.RegisterListener(SizeWorldUnitsChanged, register);
        }

        private void ValidateReferences()
        {
            if (spriteRenderer == null)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }

            _parentScaleProvider = GetComponentInParent<IParentScaleProvider>();

            Debug.Assert(spriteRenderer != null, "spriteRenderer != null");
            Debug.Assert(sizeInWorldUnitsDataSo != null, "sizeInWorldUnitsDataSo != null");
        }
    }
}
