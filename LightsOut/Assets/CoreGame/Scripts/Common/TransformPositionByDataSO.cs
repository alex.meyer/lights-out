using Sirenix.OdinInspector;
using UnityEngine;

namespace CoreGame
{
    public class TransformPositionByDataSO : MonoBehaviour
    {
        [Title("DataSOs")]
        [Required]
        [SerializeField] private Vector2DataSO positionInWorldUnitsDataSo;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            PositionWorldUnitsChanged(positionInWorldUnitsDataSo.Value);
        }

        private void OnDisable()
        {
            RegisterListeners(false);
        }

        private void PositionWorldUnitsChanged(Vector2 newPositionInWorldUnits)
        {
            transform.position = newPositionInWorldUnits;
        }

        private void RegisterListeners(bool register)
        {
            positionInWorldUnitsDataSo.RegisterListener(PositionWorldUnitsChanged, register);
        }

        private void ValidateReferences()
        {
            Debug.Assert(positionInWorldUnitsDataSo != null, "positionInWorldUnitsDataSo != null");
        }
    }
}
