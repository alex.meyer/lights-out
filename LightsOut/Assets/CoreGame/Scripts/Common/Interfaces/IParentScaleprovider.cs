using UnityEngine;

namespace CoreGame
{
    public interface IParentScaleProvider
    {
        public Vector2 Scale { get; }
    }
}