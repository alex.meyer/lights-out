using UnityEngine;

namespace CoreGame
{
    public class ScaleProvider : MonoBehaviour, IParentScaleProvider
    {
        public Vector2 Scale => transform.localScale;
    }
}