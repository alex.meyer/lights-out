using UnityEngine;
using UnityEngine.InputSystem;

namespace CoreGame
{
    public class InputManagerTouch : MonoBehaviour
    {
        [SerializeField] private bool debug;

        [SerializeField] private CameraDataSO mainCameraDataSO;
        [SerializeField] private Vector2EventSO startTouchEventSO;
        [SerializeField] private Vector2EventSO endTouchEventSO;
        [SerializeField] private Vector2EventSO deltaTouchEventSO;
        [SerializeField] private Vector2EventSO tapHappenedEventSO;
        
        [SerializeField] private bool is2d;

        private TouchControls _touchControls;

        private bool _isTouch;

        private void Awake()
        {
            ValidateReferences();
            _touchControls = new TouchControls();
        }

        private void OnEnable()
        {
            _touchControls.Enable();
            RegisterListeners(true);
            _isTouch = false;
            if (Application.platform != RuntimePlatform.Android &&
                Application.platform != RuntimePlatform.IPhonePlayer)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnDisable()
        {
            RegisterListeners(false);
            _touchControls.Disable();
        }

        private void EndTouch(InputAction.CallbackContext ctx)
        {
            var pos = _touchControls.Touch.TouchPosition.ReadValue<Vector2>();

            if (debug) Debug.Log($"[TOUCH] EndTouch at {pos} by touch");
            
            _isTouch = false;
            endTouchEventSO.Invoke(pos);
        }

        private void StartTouch(InputAction.CallbackContext ctx)
        {
            var pos = _touchControls.Touch.TouchPosition.ReadValue<Vector2>();

            if (debug) Debug.Log($"[TOUCH] StartTouch at {pos} by touch");

            _isTouch = true;
            startTouchEventSO.Invoke(pos);
        }

        private void TapHappened(InputAction.CallbackContext obj)
        {
            var pos = _touchControls.Touch.TouchPosition.ReadValue<Vector2>();

            if (is2d)
            {
                var worldPos = mainCameraDataSO.Value.ScreenToWorldPoint(pos);
                var coll2d = Physics2D.OverlapPoint(worldPos);
                if (coll2d != null)
                {
                    var tapReceiver = coll2d.GetComponentInParent<ITapReceiver>();
                    tapReceiver?.Tapped();
                }
            }
            else
            {
                var ray = mainCameraDataSO.Value.ScreenPointToRay(pos);
                if (Physics.Raycast(ray, out var hit))
                {
                    var tapReceiver = hit.collider.GetComponentInParent<ITapReceiver>();
                    tapReceiver?.Tapped();
                }
            }

            tapHappenedEventSO.Invoke(pos);

            if (debug) Debug.Log($"[TOUCH] Tap happened at: {pos} by touch");
        }

        private void Update()
        {
            var delta = Vector2.zero;
            if (_isTouch)
            {
                delta = _touchControls.Touch.TouchDelta.ReadValue<Vector2>();
            }

            if (debug) Debug.Log($"[TOUCH] Update() with delta: {delta} by {(_isTouch ? "touch" : "mouse")}");
                
            deltaTouchEventSO.Invoke(delta);
        }

        private void RegisterListeners(bool register)
        {
            _touchControls.Touch.TouchPress.started -= StartTouch;
            _touchControls.Touch.TouchPress.canceled -= EndTouch;
            _touchControls.Touch.TouchTap.performed -= TapHappened;

            if (!register)
            {
                return;
            }

            _touchControls.Touch.TouchPress.started += StartTouch;
            _touchControls.Touch.TouchPress.canceled += EndTouch;
            _touchControls.Touch.TouchTap.performed += TapHappened;
        }

        private void ValidateReferences()
        {
            Debug.Assert(mainCameraDataSO != null, "mainCameraDataSO != null");
            Debug.Assert(startTouchEventSO != null, "startTouchEventSO != null");
            Debug.Assert(endTouchEventSO != null, "endTouchEventSO != null");
            Debug.Assert(deltaTouchEventSO != null, "deltaTouchEventSO != null");
            Debug.Assert(tapHappenedEventSO != null, "tapHappenedEventSO != null");
        }
    }
}
