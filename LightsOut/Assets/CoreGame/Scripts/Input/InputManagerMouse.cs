using Board;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CoreGame
{
    public class InputManagerMouse : MonoBehaviour
    {
        [SerializeField] private bool debug;

        [SerializeField] private CameraDataSO mainCameraDataSO;
        [SerializeField] private Vector2EventSO startTouchEventSO;
        [SerializeField] private Vector2EventSO endTouchEventSO;
        [SerializeField] private Vector2EventSO deltaTouchEventSO;
        [SerializeField] private Vector2EventSO tapHappenedEventSO;

        [SerializeField] private bool is2d;

        private MouseControls _mouseControls;

        private void Awake()
        {
            ValidateReferences();
            _mouseControls = new MouseControls();
        }

        private void OnEnable()
        {
            _mouseControls.Enable();
            RegisterListeners(true);

            if (Application.platform is RuntimePlatform.Android or RuntimePlatform.IPhonePlayer)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnDisable()
        {
            RegisterListeners(false);
            _mouseControls.Disable();
        }

        private void EndClick(InputAction.CallbackContext ctx)
        {
            var pos = _mouseControls.Mouse.MousePosition.ReadValue<Vector2>();

            if (debug) Debug.Log($"[TOUCH] EndTouch at {pos} by mouse");
            
            endTouchEventSO.Invoke(pos);
        }

        private void StartClick(InputAction.CallbackContext ctx)
        {
            var pos = _mouseControls.Mouse.MousePosition.ReadValue<Vector2>();

            if (debug) Debug.Log($"[TOUCH] StartTouch at {pos} by mouse");

            _oldMousePos = pos;
            
            startTouchEventSO.Invoke(pos);
        }

        private void MouseTapHappened(InputAction.CallbackContext obj)
        {
            var pos = _mouseControls.Mouse.MousePosition.ReadValue<Vector2>();

            if (is2d)
            {
                var worldPos = mainCameraDataSO.Value.ScreenToWorldPoint(pos);
                var coll2d = Physics2D.OverlapPoint(worldPos);
                if (coll2d != null)
                {
                    var tapReceiver = coll2d.GetComponentInParent<ITapReceiver>();
                    tapReceiver?.Tapped();
                }
            }
            else
            {
                var ray = mainCameraDataSO.Value.ScreenPointToRay(pos);
                if (Physics.Raycast(ray, out var hit))
                {
                    var tapReceiver = hit.collider.GetComponentInParent<ITapReceiver>();
                    tapReceiver?.Tapped();
                }
            }

            tapHappenedEventSO.Invoke(pos);
            
            if (debug) Debug.Log($"[TOUCH] Tap happened at: {pos}");
        }

        private Vector2 _oldMousePos;
        private void Update()
        {
            var newPos = _mouseControls.Mouse.MousePosition.ReadValue<Vector2>(); 
            var delta = newPos - _oldMousePos;
            _oldMousePos = newPos;

            if (debug) Debug.Log($"[TOUCH] Update() with delta: {delta} by mouse");
                
            deltaTouchEventSO.Invoke(delta);
        }

        private void RegisterListeners(bool register)
        {
            _mouseControls.Mouse.MousePress.started -= StartClick;
            _mouseControls.Mouse.MousePress.canceled -= EndClick;
            _mouseControls.Mouse.MouseTap.performed -= MouseTapHappened;

            if (!register)
            {
                return;
            }

            _mouseControls.Mouse.MousePress.started += StartClick;
            _mouseControls.Mouse.MousePress.canceled += EndClick;
            _mouseControls.Mouse.MouseTap.performed += MouseTapHappened;
        }

        private void ValidateReferences()
        {
            Debug.Assert(mainCameraDataSO != null, "mainCameraDataSO != null");
            Debug.Assert(startTouchEventSO != null, "startTouchEventSO != null");
            Debug.Assert(endTouchEventSO != null, "endTouchEventSO != null");
            Debug.Assert(deltaTouchEventSO != null, "deltaTouchEventSO != null");
            Debug.Assert(tapHappenedEventSO != null, "tapHappenedEventSO != null");
        }
    }
}
