using System;
using System.Collections.Generic;
using System.Diagnostics;
using DG.Tweening;
using ItchyOwl.ObjectPooling;
using CoreGame;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Board
{
    [Serializable]
    public class BoardViewData
    {
        public bool useSeed;
        public int seed = 342567;
        public Vector2Int tileDimensions = new Vector2Int(5, 5);
        public float spaceBetweenTiles = 0.1f;
        public float borderSpace = 0.05f;
        public GameObject tilePrefab;
    }

    public class BoardController : MonoBehaviour
    {
        [Required]
        [SerializeField] private Vector2DataSO boardDimensionsWorldUnitsDataSO;
        [Required]
        [SerializeField] private Vector2DataSO normInnerSlotSizeInWorldUnitsDataSO;

        [SerializeField] private SlotController[] slots;

        [SerializeField] private BoardViewData data;

        public BoardViewData Data
        {
            get => data;
            set
            {
                data = value;
                RenderView();
            }
        }


        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners(true);
            KillAllChildren();
            RenderView();
        }

        private void OnDisable()
        {
            RegisterListeners(false);
            DOTween.Sequence()
                .AppendInterval(0.033f)
                .AppendCallback(RecycleAllChildren);
        }

        private void KillAllChildren()
        {
            var allSlots = GetComponentsInChildren<SlotController>();
            foreach (var slotController in allSlots)
            {
                slotController.transform.parent = null;
                Destroy(slotController.gameObject);
            }
        }

        private void RecycleAllChildren()
        {
            foreach (var slotController in slots)
            {
                slotController.gameObject.Recycle();
            }

            slots = null;
        }

        private void RenderView()
        {
            normInnerSlotSizeInWorldUnitsDataSO.Value = Vector2.one * (1.0f - data.spaceBetweenTiles);

            RecycleAllChildren();

            slots = new SlotController[data.tileDimensions.x * data.tileDimensions.y];

            if (data.useSeed)
            {
                Random.InitState(data.seed);
            }

            var unitsPerTile = SlotSize(data.tileDimensions);
            var oddX = data.tileDimensions.x % 2 > 0f;
            var oddY = data.tileDimensions.y % 2 > 0f;

            var startX = oddX ? 1 : 0;
            var startY = oddY ? 1 : 0;
            for (var x = startX; x <= data.tileDimensions.x / 2; x++)
            {
                var posX = 0f;
                // if odd tiles in row one at 0 and the other with half size offset
                if (oddX)
                {
                    posX = x * unitsPerTile.x;
                }
                // even tiles are positioned at size offset
                else
                {
                    posX = x * unitsPerTile.x + 0.5f * unitsPerTile.x;
                }

                for (var y = startY; y <= data.tileDimensions.y / 2; y++)
                {
                    var posY = 0f;
                    if (oddY)
                    {
                        posY = y * unitsPerTile.y;
                    }
                    else
                    {
                        posY = y * unitsPerTile.y + 0.5f * unitsPerTile.y;
                    }

                    SpawnSlot(x, y, posX, posY, unitsPerTile);
                    SpawnSlot(x, -y, posX, -posY, unitsPerTile);
                    SpawnSlot(-x, y, -posX, posY, unitsPerTile);
                    SpawnSlot(-x, -y, -posX, -posY, unitsPerTile);
                }
            }

            if (!oddX && !oddY)
            {
                return;
            }

            SpawnSlot(0, 0, 0, 0, unitsPerTile);

            if (oddX)
            {
                for (var y = startY; y <= data.tileDimensions.y / 2; y++)
                {
                    var posY = 0f;
                    if (oddY)
                    {
                        posY = y * unitsPerTile.y;
                    }
                    else
                    {
                        posY = y * unitsPerTile.y + 0.5f * unitsPerTile.y;
                    }

                    SpawnSlot(0, y, 0, posY, unitsPerTile);
                    SpawnSlot(0, -y, 0, -posY, unitsPerTile);
                }
            }

            if (!oddY)
            {
                return;
            }

            for (var x = startX; x <= data.tileDimensions.x / 2; x++)
            {
                var posX = 0f;
                if (oddX)
                {
                    posX = x * unitsPerTile.x;
                }
                else
                {
                    posX = x * unitsPerTile.x + 0.5f * unitsPerTile.x;
                }

                SpawnSlot(x, 0, posX, 0, unitsPerTile);
                SpawnSlot(-x, 0, -posX, 0, unitsPerTile);
            }
        }

        private void SpawnSlot(int x, int y, float posX, float posY, Vector2 unitsPerTile)
        {
            var pos = new Vector2Int(x + data.tileDimensions.x / 2, y + data.tileDimensions.y / 2);
            var go = data.tilePrefab.Spawn();
            go.name = $"{data.tilePrefab.name} [{x},{y}][{pos.x},{pos.y}]";
            var trans = go.transform;
            trans.parent = transform;
            trans.localPosition = new Vector3(posX, posY);
            trans.localScale = unitsPerTile;
            var slotCtrl = go.GetComponent<SlotController>();
            slotCtrl.Data = new SlotControllerViewData()
            {
                on = Random.Range(0, 2) == 1,
                tapped = SlotTapped,
                position = pos,
            };
            var index = ToIndex(pos);
            if (index >= 0)
            {
                slots[index] = slotCtrl;
            }
        }

        private Vector2 SlotPosition(Vector2Int coords)
        {
            var gridSlotSizeWithSpace = SlotSize(data.tileDimensions);
            var gridSlotPosition = Vector2.Scale(coords, gridSlotSizeWithSpace);
            var offset = new Vector2(gridSlotSizeWithSpace.x * data.tileDimensions.x * 0.5f, gridSlotSizeWithSpace.y * data.tileDimensions.y * 0.5f);
            offset -= gridSlotSizeWithSpace * 0.5f;
            gridSlotPosition -= offset;
            return gridSlotPosition;
        }

        private Vector2 SlotSize(Vector2Int count)
        {
            var innerBoardSize =
                boardDimensionsWorldUnitsDataSO.Value - Vector2.one * data.borderSpace;

            var size = innerBoardSize.x / count.x;

            if (size * count.y > innerBoardSize.y)
            {
                size = innerBoardSize.y / count.y;
            }

            //size -= data.spaceBetweenTiles * transform.localScale.x;
            size /= transform.localScale.x;

            return new Vector2(size, size);
        }

        private bool InRange(Vector2Int pos)
        {
            return pos.x >= 0 && pos.x < data.tileDimensions.x &&
                   pos.y >= 0 && pos.y < data.tileDimensions.y;
        }

        private SlotController GetSlot(Vector2Int pos)
        {
            var index = ToIndex(pos);
            return index >= 0 ? slots[index] : null;
        }

        private int ToIndex(Vector2Int position)
        {
            if (!InRange(position))
            {
                return -1;
            }
            return position.y * data.tileDimensions.x + position.x;
        }

        private Vector2Int ToPosition(int index)
        {
            var pos = Vector2Int.zero;
            pos.y = index / data.tileDimensions.x;
            pos.x = index - pos.y * data.tileDimensions.x;
            return pos;
        }

        private void SlotTapped(SlotController slotController)
        {
            SwitchSlot(slotController);
            var pos = slotController.Data.position;
            SwitchSlot(GetSlot(pos + Vector2Int.up));
            SwitchSlot(GetSlot(pos + Vector2Int.down));
            SwitchSlot(GetSlot(pos + Vector2Int.right));
            SwitchSlot(GetSlot(pos + Vector2Int.left));
        }

        private void SwitchSlot(SlotController slot)
        {
            if (slot == null)
            {
                return;
            }
            slot.On = !slot.On;
        }

        private void BoardDimensionsChanged(Vector2 newDimensions)
        {
            RenderView();
        }

        private void RegisterListeners(bool register)
        {
            boardDimensionsWorldUnitsDataSO.RegisterListener(BoardDimensionsChanged, register);
        }

        private void ValidateReferences()
        {
            Debug.Assert(boardDimensionsWorldUnitsDataSO != null, "boardDimensionsWorldUnitsDataSO != null");
            Debug.Assert(normInnerSlotSizeInWorldUnitsDataSO != null, "NormInnerSlotSizeInWorldUnitsDataSO != null");
        }
    }
}