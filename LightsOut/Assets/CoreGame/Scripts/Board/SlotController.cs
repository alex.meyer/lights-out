using System;
using CoreGame;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Board
{
    [Serializable]
    public class SlotControllerViewData
    {
        public bool on;
        public Action<SlotController> tapped;
        public Vector2Int position;
    }

    public class SlotController : MonoBehaviour, ITapReceiver
    {
        [SerializeField, Required] private SpriteRenderer onSprite;
        [SerializeField, Required] private SpriteRenderer offSprite;

        [SerializeField] private SlotControllerViewData _data;

        public SlotControllerViewData Data
        {
            get => _data;
            set
            {
                _data = value;
                RenderView();
            }
        }

        public bool On
        {
            get => _data.on;
            set
            {
                _data.on = value;
                RenderView();
            }
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void RenderView()
        {
            onSprite.gameObject.SetActive(_data.on);
            offSprite.gameObject.SetActive(!_data.on);
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            Debug.Assert(onSprite != null, "onSprite != null");
            Debug.Assert(offSprite != null, "offSprite != null");
        }

        public void Tapped()
        {
            Debug.Log($"[SLOT] {gameObject.name} Tapped");

            _data.tapped?.Invoke(this);
        }
    }
}
