using UnityEngine;

namespace CoreGame
{
    [CreateAssetMenu(fileName = "NewRectDataSO", menuName = "DataSO/Data/RectDataSO", order = 0)]
    public class RectDataSO : TDataObserverSO<Rect> { }
}