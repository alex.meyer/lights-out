using System;
using ItchyOwl.Extensions;
using ItchyOwl.Propagators;
using UnityEngine;

namespace CoreGame
{
    [CreateAssetMenu(fileName = "NewCameraDataSO", menuName = "DataSO/Data/CameraDataSO", order = 0)]
    public class CameraDataSO : TDataObserverSO<Camera>
    {
        public override void Set(Camera cam)
        {
            if (Value != null)
            {
                Value.GetComponent<OnDestroyPropagatorN>().Destroyed -= Destroyed;
            }

            base.Set(cam);
            cam.gameObject.GetOrAddComponent<OnDestroyPropagatorN>().Destroyed += Destroyed;
        }

        private void Destroyed(object sender, EventArgs empty)
        {
            base.Set(null);
        }
    }
}