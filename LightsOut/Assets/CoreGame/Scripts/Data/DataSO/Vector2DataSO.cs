using UnityEngine;

namespace CoreGame
{
    [CreateAssetMenu(fileName = "NewVector2DataSO", menuName = "DataSO/Data/Vector2DataSO", order = 0)]
    public class Vector2DataSO : TDataObserverSO<Vector2> { }
}
