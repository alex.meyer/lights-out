using UnityEngine;

namespace CoreGame
{
    [CreateAssetMenu(fileName = "NewVector3DataSO", menuName = "DataSO/Data/Vector3DataSO", order = 0)]
    public class Vector3DataSO : TDataObserverSO<Vector3> { }
}
