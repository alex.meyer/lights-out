using System;
using System.Collections.Generic;
using UnityEngine;

namespace CoreGame
{
    public abstract class ObserverSO : ScriptableObject
    {
        [SerializeField, TextArea] private string description;

        #if UNITY_EDITOR
        [SerializeField] private List<ObserverSO> allObserverSos;
        #endif
        
        private static readonly Dictionary<string, ObserverSO> AllObserverSOs = new Dictionary<string, ObserverSO>();

        protected virtual void OnEnable()
        {
            AllObserverSOs[name] = this;
        }

        public static ObserverSO GetByName(string observerName)
        {
            return AllObserverSOs.TryGetValue(observerName, out var observerSO) ? observerSO : null;
        }

        protected void OnValidate()
        {
            #if UNITY_EDITOR
            AllObserverSOs[name] = this;
            allObserverSos.Clear();
            foreach (var observerSOPair in AllObserverSOs)
            {
                allObserverSos.Add(observerSOPair.Value);
            }
            #endif
        }

        public abstract void Invoke();

        public virtual Delegate[] GetListeners()
        {
            return new Delegate[] { };
        }
    }
}