using UnityEngine;

namespace CoreGame
{
    public class TDataObserverSO<T> : TObserverSO<T>
    {
        [SerializeField] private T _value;
        [SerializeField] protected bool _initializeOnLoad;
        [SerializeField] protected T _initialValue;

        protected override void OnEnable()
        {
            if (_initializeOnLoad)
            {
                _value = _initialValue;
            }
        }

        public T Value
        {
            get => _value;
            set => Set(value);
        }

        public T InitialValue => _initialValue;
        
        public virtual void Set(T newValue)
        {
            _value = newValue;
            Invoke(newValue);
        }

        public override void Invoke()
        {
            Invoke(_value);
        }

        public void Reset()
        {
            Value = _initialValue;
        }
    }
}