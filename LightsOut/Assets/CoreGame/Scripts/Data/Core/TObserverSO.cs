using System;
using UnityEngine;

namespace CoreGame
{
    public class TObserverSO<T> : ObserverSO
    {
        private Action<T> _listeners;

        public override Delegate[] GetListeners() => _listeners?.GetInvocationList();

        public virtual void Invoke(T arg)
        {
            _listeners?.Invoke(arg);
        }

        public override void Invoke()
        {
            Debug.LogWarning($"[EVENT] Called without parameter. No listener is being informed.");
        }

        public void RegisterListener(Action<T> onEventTriggered, bool register)
        {
            _listeners -= onEventTriggered;

            if (!register)
            {
                return;
            }

            _listeners += onEventTriggered;
        }
    }
}