using UnityEngine;

namespace CoreGame
{
    [CreateAssetMenu(fileName = "NewVector2EventSO", menuName = "DataSO/Events/Vector2EventSO", order = 0)]
    public class Vector2EventSO : TObserverSO<Vector2>
    {
        
    }
}