using System;

namespace CoreGame
{
    public class EventSO : ObserverSO
    {
        private Action _listeners;

        public override Delegate[] GetListeners() => _listeners?.GetInvocationList();

        public override void Invoke()
        {
            _listeners?.Invoke();
        }

        public void RegisterListener(Action action, bool register)
        {
            _listeners -= action;

            if (!register)
            {
                return;
            }

            _listeners += action;
        }
    }
}
